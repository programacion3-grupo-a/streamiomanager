namespace StreamIOManager.Utils;

/// <summary>
/// Clase que contiene constantes relacionadas con la gestión de archivos.
/// </summary>
internal static class ArchivosConstantes
{
    public static string DireccionCreacion = "C:\\Users\\EverMamaniV\\source\\repos\\StreamIOManager\\StreamIOManager\\Exportar\\";
    public static string LogFilePath = "C:\\Users\\EverMamaniV\\source\\repos\\StreamIOManager\\StreamIOManager\\Log\\log.txt";
    public static Dictionary<string, string> rutasArchivos = new Dictionary<string, string>()
        {
            { "ListaCompras", "C:\\Users\\EverMamaniV\\source\\repos\\StreamIOManager\\StreamIOManager\\Archivos\\listaCompras.txt" },
            { "ListaMaterias", "C:\\Users\\EverMamaniV\\source\\repos\\StreamIOManager\\StreamIOManager\\Archivos\\listaMaterias.txt" },
            { "ListaTareas", "C:\\Users\\EverMamaniV\\source\\repos\\StreamIOManager\\StreamIOManager\\Archivos\\listaTareas.txt" }
        };

    /// <summary>
    /// Obtiene la ruta del archivo asociado al nombre especificado.
    /// </summary>
    /// <param name="nombreArchivo">Nombre del archivo.</param>
    /// <returns>Ruta del archivo.</returns>
    public static string ObtenerRuta(string nombreArchivo)
    {
        if (rutasArchivos.ContainsKey(nombreArchivo))
        {
            return rutasArchivos[nombreArchivo];
        }
        else
        {
            throw new ArgumentException("El nombre de archivo proporcionado no existe en la lista.");
        }
    }
}
