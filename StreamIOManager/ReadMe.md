# StreamIOManager

StreamIOManager es una aplicación de gestión de archivos que permite importar y exportar datos desde y hacia archivos de texto.

## Características

- **Importar datos:** Permite importar datos desde archivos de texto existentes en el sistema de archivos.
- **Exportar datos:** Permite exportar datos a archivos de texto, creando nuevos archivos o sobrescribiendo los existentes.
- **Gestión de excepciones:** Maneja excepciones que puedan ocurrir durante el proceso de importación y exportación, registrándolas en un archivo de registro.
- **Interfaz de usuario intuitiva:** Proporciona una interfaz gráfica simple y fácil de usar para interactuar con las funcionalidades de importación y exportación.

## Tecnologías utilizadas

- **C#:** El backend de la aplicación está desarrollado en C# para la lógica de negocio y el manejo de archivos.
- **XAML:** Se utiliza XAML para definir la interfaz de usuario en la parte de frontend de la aplicación.
- **MVVM:** La arquitectura Modelo-Vista-VistaModelo se emplea para separar claramente la lógica de presentación de la lógica de negocio.

## Estructura del proyecto

El proyecto está organizado en los siguientes componentes:

- **MainViewModel:** Representa el ViewModel principal que actúa como puente entre la vista (interfaz de usuario) y el modelo (lógica de negocio).
- **Model:** Contiene las clases que definen la lógica de negocio, como la importación y exportación de archivos, y la gestión de excepciones.
- **Utils:** Contiene clases utilitarias, como constantes relacionadas con la gestión de archivos.
- **Archivo:** Contiene los archivos de texto que se importan.
- **Exportar:** Se crea el archivo de texto que contiene los datos exportados.
- **Log:** Contiene el archivo de registro que registra las excepciones que ocurren durante la importación y exportación de archivos.
- 
## Requisitos del sistema

Para ejecutar la aplicación, es necesario tener instalado el entorno de ejecución de .NET.

