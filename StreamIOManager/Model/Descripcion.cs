namespace StreamIOManager.Model;

/// <summary>
/// Clase que representa una descripción.
/// </summary>
internal class Descripcion
{
    public string Texto { get; set; }

    public Descripcion(string texto)
    {
        Texto = texto;
    }
}
