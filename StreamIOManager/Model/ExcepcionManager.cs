using StreamIOManager.Utils;
namespace StreamIOManager.Model;

/// <summary>
/// Clase encargada de gestionar excepciones.
/// </summary>
internal class ExcepcionManager
{
    /// <summary>
    /// Registra una excepción en el archivo de registro.
    /// </summary>
    /// <param name="ex">Excepción a registrar.</param>
    public void RegistrarExcepcion(Exception ex)
    {
        string mensaje = $"[{DateTime.Now}] {ex.GetType().Name}: {ex.Message}";
        File.AppendAllText(ArchivosConstantes.LogFilePath, mensaje + Environment.NewLine);
    }
}
