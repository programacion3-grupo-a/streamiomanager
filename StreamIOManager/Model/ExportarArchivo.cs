using StreamIOManager.Utils;
namespace StreamIOManager.Model;

/// <summary>
/// Clase encargada de exportar datos a archivos.
/// </summary>
internal class ExportarArchivo
{
    private int contador = 1; // Contador para generar nombres de archivo únicos

    /// <summary>
    /// Exporta los datos proporcionados a un archivo de texto.
    /// </summary>
    /// <param name="datos">Datos a exportar.</param>
    public void Exportar(IEnumerable<string> datos)
    {
        try
        {
            string nombreArchivo = $"archivo{contador}.txt";
            string rutaCompleta = Path.Combine(ArchivosConstantes.DireccionCreacion, nombreArchivo);

            while (File.Exists(rutaCompleta))
            {
                contador++;
                nombreArchivo = $"archivo{contador}.txt";
                rutaCompleta = Path.Combine(ArchivosConstantes.DireccionCreacion, nombreArchivo);
            }

            using (StreamWriter sw = new StreamWriter(rutaCompleta))
            {
                foreach (string dato in datos)
                {
                    sw.WriteLine(dato);
                }
            }
        }
        catch (Exception ex)
        {
            ExcepcionManager excepcionManager = new ExcepcionManager();
            excepcionManager.RegistrarExcepcion(ex);
        }
    }
}
