namespace StreamIOManager.Model;
using System.IO;

/// <summary>
/// Clase encargada de importar datos desde un archivo.
/// </summary>
internal class ImportarArchivo
{
    /// <summary>
    /// Importa los datos desde un archivo.
    /// </summary>
    /// <param name="rutaArchivo">Ruta del archivo a importar.</param>
    /// <returns>Arreglo de líneas leídas desde el archivo.</returns>
    public string[] Importar(string rutaArchivo)
    {
        List<string> lineas = new List<string>();

        try
        {
            if (File.Exists(rutaArchivo))
            {
                using (StreamReader sr = new StreamReader(rutaArchivo))
                {
                    string linea;
                    while ((linea = sr.ReadLine()) != null)
                    {
                        lineas.Add(linea);
                    }
                }
            }
            else
            {
                throw new FileNotFoundException("El archivo especificado no existe.");
            }
        }
        catch (Exception ex)
        {
            ExcepcionManager excepcionManager = new ExcepcionManager();
            excepcionManager.RegistrarExcepcion(ex);
        }

        return lineas.ToArray();
    }
}
