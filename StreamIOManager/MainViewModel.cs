using System.Collections.ObjectModel;
using System.ComponentModel;
using StreamIOManager.Model;
using StreamIOManager.Utils;

namespace StreamIOManager;

/// <summary>
/// ViewModel principal para la gestión de la aplicación.
/// </summary>
internal class MainViewModel : INotifyPropertyChanged
{
    public ObservableCollection<string> Descripciones { get; set; }
    public ICommand ImportarCommand { get; set; }
    public ICommand ExportarCommand { get; set; }

    private readonly ImportarArchivo importador;
    private readonly ExportarArchivo exportador;
    private readonly ExcepcionManager excepcionManager;
    private ObservableCollection<string> _archivosEnCarpeta;

    public ObservableCollection<string> ArchivosEnCarpeta
    {
        get { return _archivosEnCarpeta; }
        set
        {
            if (_archivosEnCarpeta != value)
            {
                _archivosEnCarpeta = value;
                OnPropertyChanged(nameof(ArchivosEnCarpeta));
            }
        }
    }

    private string _rutaArchivoSeleccionado;

    public string RutaArchivoSeleccionado
    {
        get { return _rutaArchivoSeleccionado; }
        set
        {
            if (_rutaArchivoSeleccionado != value)
            {
                _rutaArchivoSeleccionado = value;
                OnPropertyChanged(nameof(RutaArchivoSeleccionado));
            }
        }
    }

    private string _estadoProceso;

    public string EstadoProceso
    {
        get { return _estadoProceso; }
        set
        {
            if (_estadoProceso != value)
            {
                _estadoProceso = value;
                OnPropertyChanged(nameof(EstadoProceso));
            }
        }
    }

    public MainViewModel()
    {
        Descripciones = new ObservableCollection<string>();
        importador = new ImportarArchivo();
        exportador = new ExportarArchivo();
        excepcionManager = new ExcepcionManager();

        ImportarCommand = new RelayCommand<string>(Importar);
        ExportarCommand = new RelayCommand<string>(Exportar);

        Descripciones.Add("Lista 1");
        Descripciones.Add("Lista 2");
        Descripciones.Add("Lista 3");

        ActualizarArchivosEnCarpeta();
        EstadoProceso = "Lista";
    }

    /// <summary>
    /// Método para actualizar los archivos disponibles en la carpeta.
    /// </summary>
    public void ActualizarArchivosEnCarpeta()
    {
        try
        {
            var nombresArchivos = new ObservableCollection<string>(ArchivosConstantes.rutasArchivos.Keys);
            ArchivosEnCarpeta = nombresArchivos;
        }
        catch (Exception ex)
        {
            excepcionManager.RegistrarExcepcion(ex);
        }
    }

    /// <summary>
    /// Método para importar datos desde un archivo.
    /// </summary>
    /// <param name="nombreArchivo">Nombre del archivo a importar.</param>
    private void Importar(string nombreArchivo)
    {
        try
        {
            if (nombreArchivo == null)
            {
                EstadoProceso = "Exception se guardo en el archivo log.txt ";
                throw new ArgumentNullException(nameof(nombreArchivo), "El nombre del archivo es nulo.");
            }

            string filePath = ArchivosConstantes.ObtenerRuta(nombreArchivo);
            string[] lineas = importador.Importar(filePath);
            Descripciones.Clear();
            foreach (string linea in lineas)
            {
                Descripciones.Add(linea);
            }
            EstadoProceso = "Importación exitosa";
            RutaArchivoSeleccionado = filePath;
        }
        catch (IOException ex)
        {
            excepcionManager.RegistrarExcepcion(ex);
        }
        catch (ArgumentException ex)
        {
            excepcionManager.RegistrarExcepcion(ex);
        }
    }

    /// <summary>
    /// Método para exportar los datos a un archivo.
    /// </summary>
    /// <param name="parametro">Parámetro de exportación.</param>
    private void Exportar(object parametro)
    {
        try
        {
            exportador.Exportar(Descripciones);
            EstadoProceso = "Exportación exitosa";
        }
        catch (IOException ex)
        {
            excepcionManager.RegistrarExcepcion(ex);
        }
    }

    public event PropertyChangedEventHandler PropertyChanged;
    protected void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
